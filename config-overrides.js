const { override, useEslintRc, addLessLoader } = require('customize-cra');
const path = require('path');

module.exports = override(
  useEslintRc(path.resolve(__dirname, '.eslintrc.js')),
  addLessLoader({ strictMath: true, noIeCompat: true, localIdentName: '[local]--[hash:base64:5]' }),
);
