/* eslint-disable camelcase */
/* eslint-disable react/jsx-filename-extension */
import i18next from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import React from 'react';
import ReactDOM from 'react-dom';
import { I18nextProvider } from 'react-i18next';
import App from './App';
import './index.css';
import './libs/skeleton/normalize.css';
import './libs/skeleton/skeleton.css';
import { unregister as unregisterServiceWorker } from './serviceWorker';

const common_es = require('./translations/es/common.json');
const common_en = require('./translations/en/common.json');

const resources = {
  en: common_en,
  es: common_es,
};

i18next
  .use(LanguageDetector)
  .init({
    interpolation: { escapeValue: false },
    react: { useSuspense: false },
    lng: 'es',
    fallbackLng: ['es', 'en'],
    resources,
    transKeepBasicHtmlNodesFor: ['br', 'strong', 'i'],
  });

ReactDOM.render(
  <I18nextProvider i18n={i18next}>
    <App />
  </I18nextProvider>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
unregisterServiceWorker();
