/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { Component } from 'react';
import './styles.less';
import '../../css/utils.less';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons';
import { faChevronUp } from '@fortawesome/free-solid-svg-icons';
import MenuItem from '../MenuItem/MenuItem';

class Header extends Component {
  constructor(props) {
    super(props);

    this.scrollToTop = this.scrollToTop.bind(this);

    this.sections = {
      home: null,
      about: null,
      contact: null,
    };

    this.state = {};
  }

  componentDidUpdate() {
    this.getAnchorPoints();
  }

  getAnchorPoints() {
    const curScroll = window.scrollY;

    Object.keys(this.sections).forEach((key) => {
      this.sections[key] = document.getElementById(key).getBoundingClientRect().top + curScroll;
    });
  }

  // eslint-disable-next-line class-methods-use-this
  scrollToTop() {
    const target = document.getElementById('Purrpies-Logo');
    target.scrollIntoView({ behavior: 'smooth', block: 'start' });
  }

  render() {
    const menuItems = [];

    Object.keys(this.sections).forEach((section) => {
      menuItems.push(<MenuItem key={section} menuClass="Footer-menuItem" menuAnchorClass="Footer-menuAnchor" uppercased itemName={section} />);
    });

    return (
      <div className="Footer">
        <div className="Footer-row">
          <div onClick={this.scrollToTop} className="Footer-scrollToTopContainer">
            <FontAwesomeIcon className="Footer-scrollToTopIcon" icon={faChevronUp} />
          </div>
          <div className="row">
            <div className="twelve columns">
              <div className="Footer-rowContainer">
                <img className="Footer-brandLogo" src="../pp-logo-green.png" alt="pp-logo-green" />
              </div>
              <div className="Footer-rowContainer">
                <div className="Footer-social">
                  <a href="https://www.facebook.com/purrpies">
                    <FontAwesomeIcon icon={faFacebook} className="Footer-socialIcon" />
                  </a>
                </div>
                <div className="Footer-social">
                  <a href="https://twitter.com/purrpies">
                    <FontAwesomeIcon icon={faTwitter} className="Footer-socialIcon" />
                  </a>
                </div>
                <div className="Footer-social">
                  <a href="https://www.instagram.com/purrpiesok">
                    <FontAwesomeIcon icon={faInstagram} className="Footer-socialIcon" />
                  </a>
                </div>
              </div>
              <div className="Footer-rowContainer">
                <ul className="Footer-menu">
                  {menuItems}
                </ul>
              </div>
              <div className="Footer-rowContainer">
                <a href="/terms">Términos y Condiciones</a>
              </div>
              <div className="Footer-rowContainer">
                <a href="/policy">Política de Privacidad y Protección de Datos</a>
              </div>
              <div className="Footer-rowContainer">
                <a href="/eula">EULA</a>
              </div>
            </div>
          </div>
        </div>
        <div className="Footer-row Footer-copyrights">
          <div className="row">
            <div className="twelve columns">
              <span className="Footer-copyrightsText">Purrpies 2019 @ All rights reserved</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
