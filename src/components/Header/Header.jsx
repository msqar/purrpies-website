/* eslint-disable no-debugger */
/* eslint-disable no-console */
import React, { Component } from 'react';
import './styles.less';
import '../../css/utils.less';
import PropTypes from 'prop-types';
// import i18n from 'i18next';
// import HamburgerMenu from 'react-hamburger-menu';
import MenuItem from '../MenuItem/MenuItem';

class Header extends Component {
  constructor(props) {
    super(props);

    this.sections = {
      home: null,
      about: null,
      contact: null,
    };

    this.handleLanguageChange = this.handleLanguageChange.bind(this);
    // this.openHamburgerMenu = this.openHamburgerMenu.bind(this);

    this.state = {
      isVisible: true,
      // open: false,
    };
  }

  componentDidMount() {
    this.setState({
      isVisible: window.location.pathname === '/',
    });
  }

  componentDidUpdate() {
    const { isVisible } = this.state;
    if (isVisible) {
      this.getAnchorPoints();
    }
  }

  getAnchorPoints() {
    const curScroll = window.scrollY;

    Object.keys(this.sections).forEach((key) => {
      this.sections[key] = document.getElementById(key).getBoundingClientRect().top + curScroll;
    });
  }

  handleLanguageChange(event) {
    const { onLanguageChange } = this.props;

    if (onLanguageChange) {
      onLanguageChange(event.target.value);
    }
  }

  // openHamburgerMenu() {
  //   const { open } = this.state;
  //   this.setState({ open: !open });
  // }

  render() {
    const menuItems = [];
    // const currentLng = i18n.language || window.localStorage.i18nextLng || '';

    Object.keys(this.sections).forEach((section) => {
      menuItems.push(<MenuItem menuClass="Header-menuItem" menuAnchorClass="Header-menuAnchor" key={section} itemName={section} />);
    });

    const { isVisible } = this.state;
    return (
      <div className="Header">
        <div className="container">
          <div className="row">
            <div className="one-half column">
              <a href="/">
                <img id="Purrpies-Logo" alt="purrpies-logo" className="Header-logo" src="../purrpies-logo.png" />
              </a>
            </div>
            <div className="one-half column">
              {/* Header-desktop */}
              <ul className="Header-menu">
                { isVisible ? menuItems : null }
                {/* <li>
                  <select defaultValue={currentLng} className="Header-language" id="lang" onChange={this.handleLanguageChange}>
                    <option value="es">ES</option>
                    <option value="en">EN</option>
                  </select>
                </li> */}
              </ul>
              {/* <div className="Header-menu Header-mobile">
                <HamburgerMenu
                  isOpen={open}
                  menuClicked={this.openHamburgerMenu}
                  width={18}
                  height={15}
                  strokeWidth={2}
                  rotate={0}
                  color="white"
                  borderRadius={0}
                  animationDuration={0.5}
                />
                <ul className="Header-menu">
                  { isVisible ? menuItems : null }
                </ul>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Header.defaultProps = {
  onLanguageChange: () => undefined,
};

Header.propTypes = {
  onLanguageChange: PropTypes.func,
};

export default Header;
