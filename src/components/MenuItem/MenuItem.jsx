import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../Header/styles.less';
import { Translation } from 'react-i18next';

class MenuItem extends Component {
  constructor(props) {
    super(props);
    this.anchorTarget = null;
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    const { itemName } = this.props;
    this.anchorTarget = document.getElementById(itemName);
  }

  handleClick(e) {
    e.preventDefault();
    this.anchorTarget.scrollIntoView({ behavior: 'smooth', block: 'start' });
  }

  render() {
    const {
      itemName,
      uppercased,
      menuClass,
      menuAnchorClass,
    } = this.props;

    return (
      <li className={menuClass}>
        <a className={menuAnchorClass} href={`#${itemName}`} onClick={this.handleClick} aria-label={`Scroll to ${itemName}`}>
          <Translation>
            {
              (t) => (!uppercased ? t(`${itemName}`) : t(`${itemName}`).toUpperCase())
            }
          </Translation>
        </a>
      </li>
    );
  }
}

MenuItem.defaultProps = {
  itemName: 'home',
  uppercased: false,
  menuClass: '',
  menuAnchorClass: '',
};

MenuItem.propTypes = {
  itemName: PropTypes.string,
  uppercased: PropTypes.bool,
  menuClass: PropTypes.string,
  menuAnchorClass: PropTypes.string,
};

export default MenuItem;
