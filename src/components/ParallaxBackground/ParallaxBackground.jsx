import React, { Component } from 'react';
import './styles.less';

class ParallaxBackground extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div className="ParallaxImage">
        <div className="ParallaxImage-item" />
      </div>
    );
  }
}

export default ParallaxBackground;
