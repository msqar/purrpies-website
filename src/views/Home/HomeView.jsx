/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable react/prop-types */
import axios from 'axios';
import React from 'react';
import { Trans, withTranslation } from 'react-i18next';
import LogoAndroid from 'react-ionicons/lib/LogoAndroid';
import LogoApple from 'react-ionicons/lib/LogoApple';
import MdOptions from 'react-ionicons/lib/MdOptions';
import MdPaw from 'react-ionicons/lib/MdPaw';
import MdPerson from 'react-ionicons/lib/MdPerson';
import MdSearch from 'react-ionicons/lib/MdSearch';
import catPaws from '../../assets/cat-paws.png';
import downloadGoogle from '../../assets/download-google.png';
import downloadIOS from '../../assets/download-ios.png';
import Footer from '../../components/Footer/Footer';
import '../../css/utils.less';
import './styles.less';

class HomeView extends React.Component {
  constructor(props) {
    super(props);

    const DEFAULT_SUBJECT = 'reasonQuestions';

    this.state = {
      email: '',
      subject: DEFAULT_SUBJECT,
      message: '',
      messageSent: false,
      hasSubmitError: false,
      hasSubmittedForm: false,
    };

    const resetContactForm = () => {
      this.setState({
        email: '',
        subject: DEFAULT_SUBJECT,
        message: '',
        messageSent: false,
        hasSubmitError: false,
        hasSubmittedForm: false,
      });
    };

    this.onContactFormSubmit = () => {
      this.setState({
        hasSubmittedForm: true,
      });

      const { email, subject, message } = this.state;
      const { t } = this.props;

      if (email.length && message.length) {
        axios.post('/api/webContact', {
          email,
          subject: t(subject),
          message,
        }).then(() => {
          this.setState({
            messageSent: true,
          });

          setTimeout(resetContactForm, 3 * 1000);
        }).catch(() => {
          this.setState({
            hasSubmitError: true,
          });
        });
      }
    };

    this.onSubjectChange = (evt) => {
      this.setState({
        subject: evt.target.value,
      });
    };

    this.onChangeEmail = (evt) => {
      this.setState({ email: evt.target.value });
    };

    this.onChangeMessage = (evt) => {
      this.setState({
        message: evt.target.value,
      });
    };
  }

  render() {
    const { t } = this.props;

    const {
      email, subject, message, messageSent, hasSubmittedForm, hasSubmitError,
    } = this.state;


    function isValidEmail(_email) {
      // eslint-disable-next-line no-useless-escape
      return /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(_email);
    }

    return (
      <div className="Home">
        <div id="home" className="section hero">
          <div className="container">
            <div className="one-half column">
              <h3 className="section-heading">{t('hi')}</h3>
              <p className="section-description">
                <Trans i18nKey="welcome" />
              </p>
              <div className="block-spacer" />
              <h3 className="section-heading">{t('wantToJoin')}</h3>
              <a href="https://play.google.com/store/apps/details?id=com.purrpies&hl=es"><img className="Home-downloadImage" src={downloadGoogle} alt="android" /></a>
              <a href="https://www.apple.com/ios/app-store/"><img className="Home-downloadImage" src={downloadIOS} alt="ios" /></a>
            </div>
            <div className="one-half column phones">
              <img alt="phone" src="../phone.png" className="phone" />
              <img alt="phone" src="../phone2.png" className="phone" />
            </div>
          </div>
        </div>

        <div id="about" className="section about">
          <div className="container">
            <div className="top-detail-container">
              <img className="top-detail-image" alt="catpaws" src={catPaws} />
            </div>
            <h3 className="section-heading">{t('whatsAbout')}</h3>
            <p className="section-description"><Trans i18nKey="firstAboutParagraph" /></p>
            <p className="section-description">{t('secondAboutParagraph')}</p>
            <p className="section-description"><Trans i18nKey="thirdAboutParagraph" /></p>

            <div className="block-spacer" />

            <h3 className="section-heading">{t('howToStart')}</h3>
            <p className="section-description">
              <Trans i18nKey="howToStartDescription">
                <LogoApple className="inline-logo" fontSize="30px" color="#000" /><LogoAndroid className="inline-logo" fontSize="30px" color="#000" />
              </Trans>
            </p>

            <div className="block-spacer" />

            <div className="row">
              <div className="twelve columns">
                <div className="section-item">
                  <div className="three columns">
                    <img className="u-max-full-width" src="/website-account.png" alt="createAccount" />
                  </div>
                  <div className="nine columns">
                    <h5 className="section-heading align-left">{t('createAccount')}</h5>
                    <p className="section-item-description align-left">{t('createAccountDesc')}</p>
                  </div>
                </div>
              </div>
            </div>

            <div className="section-item-divider" />

            <div className="row">
              <div className="twelve columns">
                <div className="section-item reversed">
                  <div className="nine columns">
                    <h5 className="section-heading align-right">{t('addPets')}</h5>
                    <p className="section-item-description align-right">
                      <Trans i18nKey="addPetsDesc">
                        <MdPerson className="inline-logo" fontSize="30px" color="#000" />
                      </Trans>
                    </p>
                  </div>
                  <div className="three columns">
                    <img className="u-max-full-width" src="/website-addpet.png" alt="addPet" />
                  </div>
                </div>
              </div>
            </div>

            <div className="section-item-divider" />

            <div className="row">
              <div className="twelve columns">
                <div className="section-item">
                  <div className="three columns">
                    <img className="u-max-full-width" src="/website-location.png" alt="location" />
                  </div>
                  <div className="nine columns">
                    <h5 className="section-heading align-left">{t('searchByName')}</h5>
                    <p className="section-item-description align-left">
                      <Trans i18nKey="searchByNameFirstParagraph" />
                    </p>
                    <p className="section-item-description align-left">
                      <Trans i18nKey="searchByNameSecondParagraph">
                        <MdPaw className="inline-logo" fontSize="30px" color="#000" />
                        <MdSearch className="inline-logo" fontSize="30px" color="#000" />
                      </Trans>
                    </p>
                    <p className="section-item-description align-left">
                      <Trans i18nKey="searchByNameThirdParagraph">
                        <MdOptions className="inline-logo" fontSize="30px" color="#000" />
                      </Trans>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="contact" className="section contact">
          <div className="container">
            <h3 className="section-heading">{t('contactUs')}</h3>
            <div className="contact-form" action="">
              <div className={`Home-contactFormSuccess ${messageSent ? 'visible' : ''}`}>Mensaje enviado</div>
              <div className={`Home-contactFormServerError ${hasSubmitError ? 'visible' : ''}`}>Lo sentimos, el mensaje no pudo ser enviado, por favor vuelve a intentar más tarde...</div>
              <div className="row">
                <div className="six columns">
                  <label className="form-label" htmlFor="exampleEmailInput">
                    {t('email')}
                    <input className="Home-contactFormEmail form-input u-full-width" type="email" value={email} onChange={this.onChangeEmail} placeholder={t('emailPlaceholder')} id="exampleEmailInput" />
                    <span className={`Home-contactFormError ${hasSubmittedForm === false || isValidEmail(email) ? '' : 'visible'}`}>Debes ingresar un correo</span>
                  </label>
                </div>
                <div className="six columns">
                  <label className="form-label" htmlFor="exampleRecipientInput">
                    {t('emailReason')}
                    <select className="form-input u-full-width" id="exampleRecipientInput" onChange={this.onSubjectChange} value={subject}>
                      <option value="reasonQuestions">{t('reasonQuestions')}</option>
                      <option value="reasonReportProblems">{t('reasonReportProblems')}</option>
                      <option value="reasonSuggestion">{t('reasonSuggestion')}</option>
                      <option value="reasonJob">{t('reasonJob')}</option>
                    </select>
                  </label>
                </div>
              </div>
              <label className="form-label" htmlFor="exampleMessage">
                {t('message')}
                <textarea className="Home-contactFormMessage form-input u-full-width" placeholder={t('messagePlaceholder')} id="exampleMessage" onChange={this.onChangeMessage} value={message} />
                <span className={`Home-contactFormError ${hasSubmittedForm === false || message.length > 0 ? '' : 'visible'}`}>Debes escribir un mensaje</span>
              </label>
              <input className="button-primary section-button" type="button" onClick={this.onContactFormSubmit} value={t('submit')} />
            </div>
          </div>
        </div>

        <Footer />
      </div>
    );
  }
}

export default withTranslation()(HomeView);
