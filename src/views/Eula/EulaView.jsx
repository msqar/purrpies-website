/* eslint-disable max-len */
/* eslint-disable react/jsx-one-expression-per-line */
import React, { PureComponent } from 'react';
import './styles.less';

class EulaView extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  goBack = () => {
    window.location.href = '/';
  }

  render() {
    return (
      <div className="Eula">
        <div className="container u-full-width">

          <button className="Eula-goBackButton" type="button" onClick={this.goBack}>&lt; Volver</button>

          <p className="Eula-heading">EULA (Acuerdo de Licencia de Usuario Final)</p>
          <p className="Eula-subHeading">Versión vigente 01/03/2020</p>

          <p className="Eula-paragraph"><b>IMPORTANTE: Asegúrese de leer atentamente y comprender todos los derechos y restricciones que se describen en este Acuerdo de licencia de usuario final (en adelante “EULA”, por sus siglas en inglés).</b></p>

          <p className="Eula-title">01 - Acuerdo</p>
          <p>Este documento es un acuerdo entre usted y PURRPIES (en adelante, “PURRPIES”). PURRPIES cede a Ud. el software bajo licencia, siempre y cuando usted acepte todas las condiciones incluidas en el presente. Al instalar o utilizar el software, usted acepta quedar obligado por las condiciones del presente EULA. Si no está de acuerdo, absténgase de utilizar y/o instalar el software.</p>

          <p className="Eula-title">02 - Derechos de Autor</p>
          <p>El software está protegido por las leyes sobre derechos de autor. PURRPIES es titular de los derechos de propiedad intelectual del software (incluso a título no restrictivo, las imágenes, fotografías, animaciones, vídeos, música, texto integrados en el software).</p>

          <p className="Eula-title">03 - Concesión de Licencia</p>
          <p>El software no se le vende, sino que se le cede bajo licencia, y su uso está supeditado a la aceptación de este EULA. PURRPIES le otorga una licencia limitada, personal, no exclusiva y no transferible para instalar el software en su dispositivo móvil. PURRPIES concede un uso del software de conformidad con el presente EULA, sus Términos y Condiciones y sus Políticas de Privacidad.</p>

          <p className="Eula-title">04 - Restricciones</p>
          <p>Está totalmente prohibido utilizar o explotar el software, incluido su contenido, con cualquier otra finalidad que no sea la prevista en los Términos y Condiciones de PURRPIES que regulan su uso así como también en las Políticas de Privacidad de PURRPIES. En concreto, está totalmente prohibido lo siguiente:</p>

          <ol className="Eula-listSection">
            <li>Descompilar, modificar, aplicar ingeniería inversa, desensamblar y/o reproducir el software por algún otro medio.</li>
            <li>Copiar, alquilar, arrendar, sublicenciar, distribuir o mostrar públicamente el software y/o explotar comercialmente de otro modo el software.</li>
            <li>Transmitir el software a un tercero, electrónicamente o por red, desde ningún equipo o plataforma.</li>
            <li>Emplear alguna copia de archivo o de seguridad del software.</li>
            <li>Extraer o intentar extraer (en concreto utilizando robots de extracción de datos o herramientas similares de recolección de datos) datos del software.</li>
          </ol>

          <p className="Eula-title">05 - Versiones de Prueba</p>
          <p>Si el software se le ha facilitado para probarlo durante un periodo o número de usos limitados, usted acepta no usar el software una vez transcurrido el periodo de prueba. Admite y reconoce que el software puede incluir un código diseñado para evitar que supere tales limitaciones y que dicho código podrá permanecer en su equipo una vez eliminado el software, para que no pueda instalar otra copia ni repetir el periodo de prueba.</p>

          <p className="Eula-title">06 - Finalización del Acuerdo</p>
          <p>Este EULA estará en vigor hasta que finalice. Usted podrá rescindir este EULA en cualquier momento, destruyendo el software. Este EULA quedará rescindido automáticamente sin previo aviso de PURRPIES, si usted incumple alguna de sus disposiciones. Todas las disposiciones del presente EULA relativas limitaciones de garantía y responsabilidad seguirán en vigor tras la rescisión.</p>

          <p className="Eula-title">07 - Exención de Garantía</p>
          <p>Usted es consciente de que utiliza el software por su cuenta y riesgo, y está de acuerdo con ello. PURRPIES no garantiza que el software, ni sus operaciones y funciones satisfagan sus necesidades, ni que el uso del mismo carezca de interrupciones o errores. PURRPIES RECHAZA TODA GARANTÍA EXPRESA O IMPLÍCITA DEL SOFTWARE. PURRPIES NO GARANTIZA NI REALIZA NINGUNA MANIFESTACIÓN RELATIVA AL USO O A LOS RESULTADOS DE USO DEL SOFTWARE EN TÉRMINOS DE CORRECCIÓN, EXACTITUD, FIABILIDAD, ACTUALIDAD, ETC.</p>

          <p className="Eula-title">08 - Responsabilidad Limitada</p>
          <p>EN NINGÚN CASO, PURRPIES SERÁ RESPONSABLE POR DAÑO ALGUNO DEL TIPO QUE FUERE (LO QUE INCLUYE, SIN LIMITACIÓN, DAÑOS POR LESIONES A PERSONAS O DAÑOS A LA PROPIEDAD, POR PÉRDIDA DE BENEFICIOS, INTERRUPCIÓN DEL NEGOCIO, PÉRDIDA DE INFORMACIÓN COMERCIAL, PÉRDIDA DE PRIVACIDAD, INCUMPLIMIENTO DE CUALQUIER OBLIGACIÓN Y NEGLIGENCIA) QUE SE DERIVE DEL USO O LA IMPOSIBILIDAD DE USAR EL SOFTWARE O QUE GUARDE RELACIÓN CON ESTO.</p>

          <p className="Eula-title">09 - Varios</p>
          <p>Si cualquier disposición o parte de este EULA se considerase ilícita, nula o no aplicable por cualquier motivo, se separará de las restantes disposiciones del EULA y no afectará en modo alguno a la validez o aplicabilidad de las mismas. Este EULA constituye el acuerdo completo entre usted y PURRPIES en relación al software.</p>
        </div>
        <div className="Footer-row Footer-copyrights">
          <div className="row">
            <div className="twelve columns">
              <span className="Footer-copyrightsText">Purrpies 2019 @ All rights reserved</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EulaView;
