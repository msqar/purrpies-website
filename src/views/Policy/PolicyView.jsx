/* eslint-disable max-len */
/* eslint-disable react/jsx-one-expression-per-line */
import React, { PureComponent } from 'react';
// import IosArrowBack from 'react-ionicons/lib/IosArrowBack';

import './styles.less';

class PolicyView extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  goBack = () => {
    window.location.href = '/';
  }

  render() {
    return (
      <div className="Policy">
        <div className="container u-full-width">

          <button className="Policy-goBackButton" type="button" onClick={this.goBack}>&lt; Volver</button>

          <p className="Policy-heading">Política de Privacidad y Protección de Datos</p>
          <p className="Policy-subHeading">Versión vigente 01/03/2020</p>
          <p className="Policy-paragraph">A continuación, se establece la Política de Privacidad a la que está sujeta el uso del sitio web y de la aplicación de PURRPIES (en adelante, la “Plataforma”) para Argentina.</p>
          <p className="Policy-paragraph">PURRPIES, en cumplimiento a la legislación vigente en materia de Protección de Datos LEY 25.326, través del presente documento informa a sus Usuarios la Política de Privacidad y Protección de Datos que aplicará en el tratamiento de los datos personales que el usuario facilite voluntariamente a PURRPIES al acceder y utilizar su Plataforma.</p>
          <p className="Policy-paragraph">Cualquier persona que descargue y/o utilice la Plataforma de PURRPIES declara haber leído, entendido y aceptado esta Política de Privacidad</p>
          <p className="Policy-paragraph">PURRPIES manifiesta su compromiso de cumplir en todo momento con la legislación vigente en materia de protección de datos, respetar la privacidad de los usuarios y el secreto y seguridad de los dato personales, adoptando para ello las medidas técnicas y organizativas necesarias para evitar la pérdida, mal uso, alteración, acceso no autorizado y robo de los datos personales facilitados, habida cuenta de estado de la tecnología, la naturaleza de los datos y los riesgos a los que están expuestos.</p>
          <p className="Policy-paragraph">Los datos personales entregados por los usuarios durante el proceso de registro o aquellos que se generen durante el uso de los servicios de PURRPIES, serán únicamente mantenidos por el tiempo necesario para la prestación del servicio así como para dar cumplimiento a cualquier normativa vigente que obligue a retenerlos a efectos de atender a las peticiones de las autoridades competentes.</p>
          <p className="Policy-paragraph">En todo momento, los datos que proporcione el usuario deberán ser reales, exactos y veraces, siendo de exclusiva responsabilidad del usuario la actualización de los mismos. El Usuario será el único responsable de los daños y perjuicios que PURRPIES o terceros pudieran sufrir como consecuencia de la falta de veracidad, inexactitud, falta de vigencia y autenticidad de los datos facilitados</p>
          <p className="Policy-paragraph">Se informa a los usuarios que los datos de carácter personal que sean facilitados en la Plataforma serán objeto de tratamiento automatizado y pasarán a ser de titularidad de PURRPIES, siendo el responsable del tratamiento a los fines de prestación de los servicios.</p>
          <p className="Policy-paragraph">PURRPIES recopila información del usuario que éste aporta en forma directa y otra que es aportada de forma indirecta mediante el uso de la Plataforma</p>
          <p className="Policy-paragraph Policy-indentBlock"><b>a) Información que los Usuarios facilitan directamente</b></p>
          <p className="Policy-paragraph Policy-indentSubBlock"><u>Datos de Registro:</u> la información que el usuario facilita cuando se crea una cuenta en la Plataforma de PURRPIES: nombre del usuario y correo electrónico</p>
          <p className="Policy-paragraph Policy-indentSubBlock"><u>Información del Perfil del Usuario:</u> la información opcional que el usuario añade en la Plataforma efectos de poder utilizar el servicio de PURRPIES. El Usuario puede ver y editar los datos personales de su perfil cuando estime oportuno. </p>
          <p className="Policy-paragraph Policy-indentSubBlock"><u>Información acerca de las comunicaciones realizadas con PURRPIES:</u> PURRPIES tendrá acceso a información que los usuarios faciliten para la resolución de dudas o quejas sobre el uso de la Plataforma.</p>
          <p className="Policy-paragraph Policy-indentBlock"><b>b) Información que los usuarios facilitan indirectamente</b></p>
          <p className="Policy-paragraph Policy-indentSubBlock"><u>Datos derivados del Uso de la Plataforma:</u> PURRPIES recolecta los datos derivados del uso de la Plataforma por parte del usuario cada vez que éste interactúa con la Plataforma</p>
          <p className="Policy-paragraph Policy-indentSubBlock"><u>Datos de la aplicación y del dispositivo:</u> PURRPIES almacena los datos del dispositivo y de la aplicación que el Usuario utiliza para acceder a los servicios.</p>
          <p className="Policy-paragraph Policy-indentSubBlock"><u>Datos sobre la Geolocalización:</u> siempre y cuando los usuarios lo autoricen, PURRPIES recogerá dato relacionados con la localización, incluyendo la localización geográfica en tiempo real del ordenador o dispositivo móvil de los usuarios. Los usuarios podrán escoger que PURRPIES no conozca su geolocalización. La mayoría de dispositivos móviles proporciona a los usuarios la opción de desactiva los servicios de localización. Dicha posibilidad suele encontrarse en el menú de configuración del dispositivo.</p>
          <p className="Policy-paragraph">Los datos de los usuarios de PURRPIES se almacenarán en los servidores de Amazon Web Services, situados en Sao Pablo, Brasil y en Virginia del Norte, Estados Unidos, contratados por PURRPIES, país con niveles inferiores al de la República Argentina en materia de protección de datos, pero en ningún caso decide sobre la finalidad del tratamiento de los datos alojados. Al descargar y usar la Plataforma de PURRPIES usted autoriza expresamente a que se efectúe dicha transferencia de información fuera de su país.</p>
          <p className="Policy-paragraph">En todo momento, el usuario tiene la facultad de ejercer el derecho de acceso a sus datos personales en forma gratuita a intervalos no inferiores a seis meses, salvo que se acredite su interés legítimo al efecto, conforme lo establecido en el artículo 14, inciso 3 de la Ley 25.326.</p>
          <p className="Policy-paragraph">Los usuarios tienen reconocidos y podrán ejercitar los derechos de acceder, rectificar y suprimir su Información Personal así como a oponerse al tratamiento de la misma y a ser informado de las cesiones llevadas a cabo, todo ello de conformidad a lo dispuesto en la normativa aplicable.</p>
          <p className="Policy-paragraph">La presente Política de Privacidad y los Términos de Uso podrán modificarse en cualquier momento. PURRPIES enviará a los usuarios avisos sobre modificaciones cuando ingresen a la Plataforma. De todas formas, PURRPIES en ningún caso modificará las políticas ni prácticas para hacerlas menos eficaces en la protección de los datos personales almacenados anteriormente</p>
          <p className="Policy-paragraph">PURRPIES podrá revelar la Información Personal de sus usuarios bajo requerimiento de la autoridades judiciales o gubernamentales competentes para efectos de investigaciones conducidas por ellas, aunque no exista una orden ni una citación ejecutiva o judicial, o por ejemplo (y sin limitación a este supuesto) cuando se trate de investigaciones de carácter penal o de fraude o las relacionadas con piratería informática o la violación de derechos de autor.</p>
          <p className="Policy-paragraph">La DIRECCION NACIONAL DE PROTECCION DE DATOS PERSONALES, Órgano de Control de la Ley 25.326, tiene la atribución de atender las denuncias y reclamos que se interpongan con relación al incumplimiento de las normas sobre protección de datos personales.</p>
          <p className="Policy-paragraph">Para hacer cualquier consulta, reclamo y/o petición relativa a tus datos personales, así como también a efectos de ejercer sus derechos relativos al acceso, rectificación y/o supresión de sus datos personales, el usuario podrá hacerlo contactándose con PURRPIES a través de correo electrónico a la casilla <a href="mailto:info@purrpies.com">info@purrpies.com</a>.</p>
        </div>
        <div className="Footer-row Footer-copyrights">
          <div className="row">
            <div className="twelve columns">
              <span className="Footer-copyrightsText">Purrpies 2019 @ All rights reserved</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PolicyView;
