import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import i18n from 'i18next';
import Header from './components/Header/Header';
import ParallaxBackground from './components/ParallaxBackground/ParallaxBackground';
import HomeView from './views/Home/HomeView';
import TermsView from './views/Terms/TermsView';
import PolicyView from './views/Policy/PolicyView';
import EulaView from './views/Eula/EulaView';

function App() {
  const changeLanguage = (lng) => {
    i18n.changeLanguage(lng);
  };

  return (
    <Router>
      <div className="App">
        <ParallaxBackground />
        <Header onLanguageChange={changeLanguage} />
      </div>

      <Switch>
        <Route path="/terms">
          <TermsView />
        </Route>
        <Route path="/policy">
          <PolicyView />
        </Route>
        <Route path="/eula">
          <EulaView />
        </Route>
        <Route path="/">
          <HomeView />
        </Route>
      </Switch>
    </Router>
  );
}

export default withTranslation()(App);
