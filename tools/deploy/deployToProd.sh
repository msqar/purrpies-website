#!/usr/bin/env bash
PP_WEB_DIR=.
DAY=$(date "+%d")
MONTH=$(date "+%m")
YEAR=$(date "+%Y") 
ZIP_FILE=ppweb.zip
SERVER_DEST_DIR=upload/${YEAR}/${MONTH}/${DAY}
KEY="-i ~/.ssh/pp-prod-1.pem"

# EC2 production instances
PROD_SERVER_1=ec2-18-228-238-18.sa-east-1.compute.amazonaws.com
#PROD_SERVER_2=ec2-52-67-233-130.sa-east-1.compute.amazonaws.com
#PROD_SERVER_3=ec2-18-228-193-185.sa-east-1.compute.amazonaws.com
#PROD_SERVER_4=ec2-18-230-74-253.sa-east-1.compute.amazonaws.com

function deployToServer() {
    ssh $KEY ubuntu@$1 "mkdir -p ${SERVER_DEST_DIR}"

    ssh $KEY ubuntu@$1 "rm ${SERVER_DEST_DIR}/${ZIP_FILE}"

    scp $KEY ./${ZIP_FILE} ubuntu@$1:~/${SERVER_DEST_DIR}

	ssh $KEY ubuntu@$1 "~/bin/deployPPWebToProd.sh"
}

########
# Init
########
cd ${PP_WEB_DIR}
npm run build
cd build
zip -r ../tools/deploy/$ZIP_FILE .
cd -
cd tools/deploy
pwd

#git stash
#git checkout master
#git archive --format=zip HEAD > tools/deploy/${ZIP_FILE}


########
# Deploy (comment out to deploy to specific instances one at a time)
########
#deployToServer $PROD_SERVER_4
deployToServer $PROD_SERVER_1


########
# Cleanup 
########
# Still at tools/deploy
rm ${ZIP_FILE} 

cd ${PP_WEB_DIR}
