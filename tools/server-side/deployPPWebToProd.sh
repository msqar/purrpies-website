PP_WEB_DIR=/var/www/purrpies.com/html
DAY=$(date "+%d")
MONTH=$(date "+%m")
YEAR=$(date "+%Y") 
ZIP_FILE=ppweb.zip
SERVER_DEST_DIR=~/upload/${YEAR}/${MONTH}/${DAY}

rm -rf ${PP_WEB_DIR}/*

cp ${SERVER_DEST_DIR}/${ZIP_FILE} ${PP_WEB_DIR}

cd ${PP_WEB_DIR}

unzip ${ZIP_FILE}

rm ${ZIP_FILE}

cd -

echo "Finished deploying to PROD."
