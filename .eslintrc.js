module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  parser: 'babel-eslint',
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    "max-len": [
      "warn",
        {
          "code": 200,
          "ignoreStrings": true,
          "ignoreUrls": true,
          "ignoreRegExpLiterals": true,
          "ignoreTemplateLiterals": true
        }
    ]
  }
};
